var log = require('../libs/log')(module);

module.exports = function(app) {
    var Tshirt = require('../models/tshirt.js');

    findAllTshirts = function(req, res) {
        log.log("GET - /tshirts");
        log.debug('listing %s', req.url);
        return Tshirt.find(function(err, tshirts) {
            if(!err) {
                return res.send(tshirts);
            } else {
                res.statusCode = 500;
                console.log('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    };

    findById = function (req, res) {
        log.info('GET - /tshirts/:id');
        return Tshirt.findById(req.params.id, function(err, tshirt) {
            if(!tshirt) {
                res.statusCode = 404;
                return res.send({error: 'not found'});
            }
            if(!err) {
                return res.send({status: 'OK', tshirt: tshirt });
            } else {
                res.statusCode = 500;
                console.log('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    };

    addShirt = function(req, res) {
        console.log('POST ---## /tshirts');
        //console.log(req);
        console.log(req.body);
        //res.send('post');
        var tshirt = new Tshirt({
            model: req.body.model,
            //images: req.body.images,
            style: req.body.style,
            size: req.body.size,
            colour: req.body.colour,
            price: req.body.price,
            summary: req.body.summary
        });

        tshirt.save(function(err) {
            if(!err) {
                console.log('TShirt created');
                return res.send({status: 'OK', tshirt: tshirt});
            } else {
                console.log(err);
                if(err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({error: 'Validation Error'});
                } else {
                    res.statusCode = 500;
                    res.send({error: 'Server Error'});
                }
                console.log('Internal error(%d): %s',res.statusCode,err.message);
            }
        });

        //res.send(tshirt);
    };

    updateTshirt = function(req, res) {
        console.log("PUT - /tshirt/:id");
        console.log(req.body);

        return Tshirt.findById(req.params.id, function (err, tshirt) {
            if(!tshirt) {
                res.statusCode = 404;
                res.send( {error: 'not found'});
            }

            if (req.body.model != null) tshirt.model = req.body.model;
            if (req.body.price != null) tshirt.price = req.body.price;
            if (req.body.images != null) tshirt.images = req.body.images;
            if (req.body.style != null) tshirt.style = req.body.style;
            if (req.body.size != null) tshirt.size = req.body.size;
            if (req.body.colour != null) tshirt.colour = req.body.colour;
            if (req.body.summary != null) tshirt.summary = req.body.summary;

            return tshirt.save(function(err) {
                if (!err) {
                    console.log('update');
                    return res.send({status: 'OK', tshirt: tshirt });
                } else {
                    if(err.name == 'ValidationError') {
                        res.statusCode = 400;
                        return res.send({ error: 'ValidationError'});
                    } else {
                        res.statusCode = 500;
                        return res.send({error: 'Server Error'});
                    }
                    console.log('Internal error(%d): %s',res.statusCode,err.message);
                }
            });
        });
    };

    deleteTshirt = function (req, res) {
        console.log('DELETE - /tshirt/:id');
        return Tshirt.findById(req.params.id, function(err, tshirt) {
            if(!tshirt) {
                res.statusCode = 404;
                return res.send({error: 'not found'});
            }

            return tshirt.remove(function(err) {
                if (!err) {
                    console.log('error remove');
                    return res.send({status: 'OK', tshirt: tshirt});
                } else {
                    res.statusCode = 500;
                    return res.send({error: 'serve error'});
                }
            });
        });
        //res.send('delete');
    };

    app.get('/tshirts', findAllTshirts);
    app.get('/tshirts/:id', findById);
    app.post('/tshirts', addShirt);
    app.put('/tshirts/:id', updateTshirt);
    app.delete('/tshirts/:id', deleteTshirt);
}