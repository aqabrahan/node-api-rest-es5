var express = require('express'),
    app = express(),
    http = require("http"),
    server = http.createServer(app),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    Promise = require('bluebird'),
    log = require('./libs/log')(module),
    mongoose = require('mongoose');

    Promise.promisifyAll(mongoose);
    mongoose.Promise = Promise;

    //app.configure(function () {
        app.use(bodyParser.urlencoded({ extended: true })); //json parsin
        app.use(methodOverride());  //HTTP Put and DELETE support
        //app.use(app.router); //simple route management

    //});
    //


    routes = require('./routes/tshirts.js')(app);

    /*app.use(function(req, res, next) {
        res.status(404);
        log.debug('not found URL %s', req.url);
        res.send({error: 'not found'});
        return;
    });

    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        log.error('Internal erro(%d): %s ', res.statusCode, err.message);
        res.send({error: err.message});
    });*/

    mongoose.connect('mongodb://127.0.0.1/tshirts', function(err, res) {
        if (err) {
            console.log('Error: connect to BD' + err);
        } else {
            console.log('Connected to BD');
        }
    });

    app.get('/', function(req, res) {
        res.send('hello wolrkds');
    });

    server.listen(3000, function() {
        console.log('node server running in localhost:3000');
    });