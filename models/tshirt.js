var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Images = new Schema({
    kind: {
        type: String,
        enum: ['thumbnail','detail'],
        require: true
    },
    url: { type: String, require: true }
});

var Tshirt = new Schema({
    model: {
        type: String,
        required: true
    },
    images: [Images],
    style: {
        type: String,
        enum: ['Casual','Vintage','Alternativa'],
        require: true
    },
    size: {
        type: String,
        enum: [36,38,40,42,44,46],
        require: true
    },
    colour: {
        type: String
    },
    price: {
        type: String,
        require: true
    },
    summary: {
        type: String
    },
    modified: {
        type: Date,
        default: Date.now
    }
});

Tshirt.path('model').validate(function(v) {
    return ((v != "") && (v != null));
});

module.exports = mongoose.model('Tshirt', Tshirt);